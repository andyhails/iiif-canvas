import React, {Component} from 'react';

/**
 * Component Class DrawingToolButton
 * adds the ability to toggle a drawing tool on the canvas.
 */
class DrawingToolButton extends Component {
  render() {
    const isDrawingMode = this.props.drawingMode;
    return (<button type="button" className={"btn btn-light " + (isDrawingMode ? 'active' : '')} onClick={this.props.handleToggle} >✏️</button>);
  }
}

export default DrawingToolButton;