import React from 'react';
import {fabric} from 'fabric-webpack';
import './Canvas.css';

class Canvas extends React.Component {

  canvas = {};

  componentDidMount() {
    this.setupCanvas();
  }

  setupCanvas = () => {
    this.canvas = new fabric.Canvas('c');
    this.canvas.selection = false;
    this.canvas.setWidth(document.body.clientWidth);
    this.canvas.setHeight(700);
    this.canvas.allowTouchScrolling = true;
    this.canvas.isDrawingMode = this.props.drawingMode;
    this.canvas.freeDrawingBrush.width = 10;
    this.canvas.calcOffset();
  };

  componentDidUpdate() {
    this.canvas.isDrawingMode = this.props.drawingMode;

    if (this.props.imageSize !== '') {
      this.addImageToCanvas(this.props.imageInfo, this.props.imageSize);
    }
  }

  addImageToCanvas = (imageInfo, imageSize) => {
    const url = imageInfo['@id'] + '/full/' + imageSize + '/0/default.jpg';
    fabric.Image.fromURL(url, (img) => {
      var dimensions = imageSize.split(',');
      img.scaleToWidth(dimensions[0]);
      img.setShadow("2px 2px 3px rgba(0, 0, 0, 0.8)");
      if (dimensions[1]) {
        img.scaleToHeight(dimensions[1]);
      }
      this.canvas.add(img);
      this.props.handleClosure();
    });
  };

  render() {
    return (
      <div className="drag-container">
        <canvas id="c" className="canvas-element"></canvas>
      </div>
    );
  }
}

export default Canvas;