import React, {Component} from 'react';
import ImageForm from "./components/ImageForm/ImageForm";
import Canvas from "./components/Canvas/Canvas";
import DrawingToolButton from './components/DrawingToolButton/DrawingToolButton';
import DataLoader from './services/DataLoader';
import './App.css'

const initialAppState = {
  imageInfo: null,
  imageSize: '',
  drawingMode: false
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = initialAppState;
  }

  handleFormSubmit = (formData, callback) => {
    if (this.state.imageInfo === null) {
      this.loadDataFromService(formData.imageUrl);
    }
    else {
      this.addImageToCanvas(formData);
      callback();
    }
  };

  loadDataFromService = (imageUrl) => {
    const loader = new DataLoader();
    loader.get(imageUrl).then(response => {
      this.setState({
        imageInfo: response,
      });
    });
  };

  addImageToCanvas = (formData) => {
    this.setState({
      imageSize: formData.imageSize ? formData.imageSize : '500,',
    });
  };

  toggleDrawing = () => {
    this.setState({drawingMode: !this.state.drawingMode});
  };

  resetApp = () => {
    this.setState(initialAppState);
  };

  render() {
    return (
      <div className="App">
        <div className="jumbotron">
          <div className="container">
            <div className="row">
              <div className="col">
                <h1>Create your own pinboard! 📌</h1>
                <DrawingToolButton handleToggle={this.toggleDrawing} drawingMode={this.state.drawingMode} />
              </div>
              <div className="col">
                <ImageForm imageInfo={this.state.imageInfo} handleSubmit={this.handleFormSubmit}/>
              </div>
            </div>
          </div>
        </div>
        <Canvas imageInfo={this.state.imageInfo} imageSize={this.state.imageSize} handleClosure={this.resetApp} drawingMode={this.state.drawingMode} />
      </div>
    );
  }
}

export default App;
