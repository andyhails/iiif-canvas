import React, {Component} from 'react';

const initialState = {
  imageUrl: '',
  imageSize: '500,'
};

class ImageForm extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Event methods
   */
  handleChange = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.handleSubmit(this.state, () => {
      this.resetForm();
    });
  };

  /**
   * Render methods
   */
  resetForm = () => {
    this.setState(initialState);
  };

  renderElementSize = () => {
    if (this.props.imageInfo === null || !this.props.imageInfo.hasOwnProperty('sizes')) {
      return '';
    }
    return (
      <div className="form-group">
        <label htmlFor="InputImageSize">Select Image size:</label>
          <select value={this.state.imageSize} name="imageSize" id="InputImageSize" className="form-control" aria-describedby="HelpImageSize" onChange={this.handleChange}>
            <option value="500,">500px width</option>
            {this.props.imageInfo.sizes.map(size => (<option key={size.width + ',' + size.height}
                                                             value={size.width + ',' + size.height}>
              {size.width}px x {size.height}px</option>))}
          </select>
          <small id="HelpImageSize" className="form-text text-muted">Select the image size you'd like to add to the canvas.</small>
      </div>
    );
  };

  render() {
    const isAddToCanvas = this.props.imageInfo !== null;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="InputImageUrl">Image URL:</label>
          <input type="text" name="imageUrl" id="InputImageUrl" className="form-control" aria-describedby="HelpImageUrl" placeholder="Enter Image URL" value={this.state.imageUrl} onChange={this.handleChange}/>
          <small id="HelpImageUrl" className="form-text text-muted">Please enter an "IIIF Image" endpoint to get started. You can add more as you go!</small>
        </div>
        {this.renderElementSize()}
        <button type="submit" className="btn btn-primary">{isAddToCanvas ? '+ Add to Canvas' : 'Get image →'}</button>

      </form>
    );
  }
}

export default ImageForm;
