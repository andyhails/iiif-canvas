/**
 * Class Dataloader is a utility class which can be used to pull in JSON data into the app.
 */
class DataLoader {
  get(url) {
    return new Promise(function (resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', url);
      req.onload = () => {
        if (req.status === 200) {
          resolve(JSON.parse(req.response));
        }
        else {
          reject(Error(req.statusText));
        }
      };
      req.onerror = function () {
        reject(Error("Network Error"));
      };
      req.send();
    });
  }

}

export default DataLoader;
