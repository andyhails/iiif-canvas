# Demo
https://screencast-o-matic.com/watch/cFQbrHqLi1

#Test URLs for IIIF images

- https://dlcs.io/iiif-img/2/1/e75d37c5-40a1-48a4-b61b-52ac7aa26849/info.json
- https://dlcs.io/iiif-img/3/2/04fbbb28-d5a7-4408-b7da-800c4e65eda3/info.json
- https://stacks.stanford.edu/image/iiif/wy534zh7137%252FSULAIR_rosette/info.json
- https://stacks.stanford.edu/image/iiif/bj570dr6911%252Fbj570dr6911_00_0001/info.json


# Code compilation
The react app was created using the recommended approach (https://reactjs.org/docs/create-a-new-react-app.html)
To compile, checkout this repo, install nodejs (https://nodejs.org/) and run the following commands from the root of this repository:

`$ npm install`

This installs all the necessary packages required.

`$ npm start`

This starts the compiler and (helpfully) open the project in your browser.
 


 


